package com.nuclominus.kroha.Screens.BabyMenu.Fragments.BabyFragment.Logic;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.R;

public class MenuBaby implements View.OnClickListener {

    public enum ORGAN {
        HEAD(1),
        EYES(2),
        NOSE(3),
        EARS(4),
        MOUTH(5),
        NECK(6),
        HANDS(7),
        CHEST(8),
        STOMACH(9),
        GROIN(10),
        LEG(11);

        private int value;

        ORGAN(int value) {
            this.value = value;
        }
    }

    private View babyView;
    private Context context;
    private LinearLayout chest, neck, stomach,
            hand_left, hand_right,
            groin, left_leg, right_leg, hair, eyes,
            mouth, nose, left_ear, right_ear;
    private OnMenuItemClickListener listener;
    private LinearLayout layoutClick;
    private ImageView img_chest, img_stomach, img_mouth, img_nose,
            img_left_ear, img_right_ear;
    private String gender;

    public MenuBaby(Context context, View babyView, OnMenuItemClickListener listener, Bundle bundle) {
        this.context = context;
        this.babyView = babyView;
        this.listener = listener;
        initBabyUI(bundle);
    }


    private void initBabyUI(Bundle bundle) {
        if (bundle != null) {
            LayoutInflater inflater = LayoutInflater.from(context);

            FrameLayout fL = (FrameLayout) babyView.findViewById(R.id.frameL_baby);
            fL.removeAllViews();

            if (bundle.getInt(context.getString(R.string.sex)) == 1) {
                layoutClick = (LinearLayout) inflater.inflate(R.layout.baby_layout_boy, null, false);
                fL.addView(layoutClick);
                Glide.with(context).load(R.drawable.boy_head).into(((ImageView) layoutClick.findViewById(R.id.iV_baby_head)));
                Glide.with(context).load(R.drawable.boy_body).into(((ImageView) layoutClick.findViewById(R.id.iV_baby_body)));
            } else if (bundle.getInt(context.getString(R.string.sex)) == 2) {
                layoutClick = (LinearLayout) inflater.inflate(R.layout.baby_layout_girl, null, false);
                fL.addView(layoutClick);
                Glide.with(context).load(R.drawable.girl_head).into(((ImageView) layoutClick.findViewById(R.id.iV_baby_head)));
                Glide.with(context).load(R.drawable.girl_body).into(((ImageView) layoutClick.findViewById(R.id.iV_baby_body)));
            }

        }

        hair = (LinearLayout) layoutClick.findViewById(R.id.ll_click_heir);
        eyes = (LinearLayout) layoutClick.findViewById(R.id.ll_click_eyes);

        chest = (LinearLayout) layoutClick.findViewById(R.id.ll_click_chest);
        neck = (LinearLayout) layoutClick.findViewById(R.id.ll_click_neck);
        stomach = (LinearLayout) layoutClick.findViewById(R.id.ll_click_stomach);
        hand_left = (LinearLayout) layoutClick.findViewById(R.id.ll_click_hand_left);
        hand_right = (LinearLayout) layoutClick.findViewById(R.id.ll_click_hand_right);
        groin = (LinearLayout) layoutClick.findViewById(R.id.ll_click_groin);
        left_leg = (LinearLayout) layoutClick.findViewById(R.id.ll_click_left_leg);
        right_leg = (LinearLayout) layoutClick.findViewById(R.id.ll_click_right_leg);
        mouth = (LinearLayout) layoutClick.findViewById(R.id.ll_click_mouth);
        nose = (LinearLayout) layoutClick.findViewById(R.id.ll_click_nose);
        left_ear = (LinearLayout) layoutClick.findViewById(R.id.ll_click_left_ear);
        right_ear = (LinearLayout) layoutClick.findViewById(R.id.ll_click_right_ear);

        img_chest = (ImageView) layoutClick.findViewById(R.id.img_ll_click_chest);
        img_stomach = (ImageView) layoutClick.findViewById(R.id.img_ll_click_stomach);
        img_mouth = (ImageView) layoutClick.findViewById(R.id.img_ll_click_mouth);
        img_nose = (ImageView) layoutClick.findViewById(R.id.img_ll_click_nose);
        img_left_ear = (ImageView) layoutClick.findViewById(R.id.img_ll_click_left_ear);
        img_right_ear = (ImageView) layoutClick.findViewById(R.id.img_ll_click_right_ear);

        dropSelect();

        chest.setOnClickListener(this);
        neck.setOnClickListener(this);
        stomach.setOnClickListener(this);
        hand_left.setOnClickListener(this);
        hand_right.setOnClickListener(this);
        groin.setOnClickListener(this);
        left_leg.setOnClickListener(this);
        right_leg.setOnClickListener(this);

        hair.setOnClickListener(this);
        eyes.setOnClickListener(this);
        mouth.setOnClickListener(this);
        nose.setOnClickListener(this);
        left_ear.setOnClickListener(this);
        right_ear.setOnClickListener(this);

    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void chooseOrgan(int view) {
        View[] views = {
                hair,
                eyes,
                nose,
                left_ear,
                mouth,
                neck,
                hand_right,
                chest,
                stomach,
                groin,
                right_leg
        };
        onClick(views[view - 1]);
    }

    @Override
    public void onClick(View v) {
        ORGAN listen = null;

        switch (v.getId()) {

            case R.id.ll_click_heir: {
                dropSelect();
                if (gender.equals("g"))
                    hair.setBackgroundResource(R.drawable.girl_hair);
                else if (gender.equals("b"))
                    hair.setBackgroundResource(R.drawable.boy_hair);
                listen = ORGAN.HEAD;
            }
            break;

            case R.id.ll_click_eyes: {
                dropSelect();
                if (gender.equals("g"))
                    eyes.setBackgroundResource(R.drawable.girl_eyes);
                else if (gender.equals("b"))
                    eyes.setBackgroundResource(R.drawable.boy_eyes);
                listen = ORGAN.EYES;
            }
            break;

            case R.id.ll_click_left_ear: {
            }

            case R.id.ll_click_right_ear: {
                dropSelect();
                if (gender.equals("g")) {
                    img_left_ear.setImageResource(R.drawable.girl_ear_left);
                    img_right_ear.setImageResource(R.drawable.girl_ear_right);
                } else if (gender.equals("b")) {
                    img_left_ear.setImageResource(R.drawable.boy_ear_left);
                    img_right_ear.setImageResource(R.drawable.boy_ear_right);
                }
                listen = ORGAN.EARS;
            }
            break;

            case R.id.ll_click_nose: {
                dropSelect();
                if (gender.equals("g")) {
                    img_nose.setImageResource(R.drawable.girl_nose);
                } else if (gender.equals("b")) {
                    img_nose.setImageResource(R.drawable.boy_nose);
                }
                listen = ORGAN.NOSE;
            }
            break;

            case R.id.ll_click_mouth: {
                dropSelect();
                if (gender.equals("g")) {
                    img_mouth.setImageResource(R.drawable.girl_mouth);
                } else if (gender.equals("b")) {
                    img_mouth.setImageResource(R.drawable.boy_mouth);
                }
                listen = ORGAN.MOUTH;
            }
            break;

            case R.id.ll_click_chest: {
                dropSelect();
                img_chest.setImageResource(R.drawable.cheked_chest);
                listen = ORGAN.CHEST;
            }
            break;

            case R.id.ll_click_neck: {
                dropSelect();
                neck.setBackgroundResource(R.drawable.cheked_neck);
                listen = ORGAN.NECK;
            }

            break;

            case R.id.ll_click_stomach: {
                dropSelect();
                img_stomach.setImageResource(R.drawable.cheked_stomach);
                listen = ORGAN.STOMACH;
            }
            break;

            case R.id.ll_click_hand_left: {
            }

            case R.id.ll_click_hand_right: {
                dropSelect();
                hand_right.setBackgroundResource(R.drawable.cheked_hand_right);
                hand_left.setBackgroundResource(R.drawable.cheked_hand_left);
                listen = ORGAN.HANDS;
            }
            break;

            case R.id.ll_click_groin: {
                dropSelect();
                groin.setBackgroundResource(R.drawable.cheked_groin);
                listen = ORGAN.GROIN;
            }
            break;

            case R.id.ll_click_left_leg: {

            }

            case R.id.ll_click_right_leg: {
                dropSelect();
                right_leg.setBackgroundResource(R.drawable.right_leg);
                left_leg.setBackgroundResource(R.drawable.left_leg);
                listen = ORGAN.LEG;
            }
            break;

        }
        listener.OnClickMenuItem(listen.value);
    }

    public void dropSelect() {
        img_stomach.setImageBitmap(null);
        img_chest.setImageBitmap(null);

        img_mouth.setImageBitmap(null);
        img_nose.setImageBitmap(null);
        img_left_ear.setImageBitmap(null);
        img_right_ear.setImageBitmap(null);

        hair.setBackgroundResource(android.R.color.transparent);
        eyes.setBackgroundResource(android.R.color.transparent);
        left_leg.setBackgroundResource(android.R.color.transparent);
        right_leg.setBackgroundResource(android.R.color.transparent);
        hand_left.setBackgroundResource(android.R.color.transparent);
        hand_right.setBackgroundResource(android.R.color.transparent);
        groin.setBackgroundResource(android.R.color.transparent);
        neck.setBackgroundResource(android.R.color.transparent);
    }


}
