package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ABCListFragment;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Adapters.ABCListAdapter;
import com.nuclominus.kroha.Screens.BabyMenu.Items.ABC_Item;
import com.nuclominus.kroha.Utils.AppConsts;

import java.util.ArrayList;

public class ABCListFragment extends ParentListFragment {

    private ArrayList<ABC_Item> listArray;
    private ABCListAdapter adapter;

    public static ABCListFragment newInstance() {
        ABCListFragment fragment = new ABCListFragment();
        return fragment;
    }

    public void setAdapter() {
        String[] organs = new String[0];
        if (listArray == null) {
            listArray = new ArrayList<>();

            organs = new String[]{
                    getString(R.string.string_organ_hear),
                    getString(R.string.string_organ_eyes),
                    getString(R.string.string_organ_nose),
                    getString(R.string.string_organ_ears),
                    getString(R.string.string_organ_mouth),
                    getString(R.string.string_organ_neck),
                    getString(R.string.string_organ_arms),
                    getString(R.string.string_organ_chest),
                    getString(R.string.string_organ_abdomen),
                    getString(R.string.string_organ_genitalia),
                    getString(R.string.string_organ_legs),
                    getString(R.string.string_organ_skin)
            };
        }
        setData(listArray, organs);

        adapter = new ABCListAdapter(listArray, getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Nullable
    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_abclist, container, false);
    }

    @Override
    public void OnListItemClick(int parent_id) {
        bundle.putInt(AppConsts.PARENT_ID, parent_id + 1);
        onMenuItemClickListener.OnClickMenuItem(3);
    }
}
