package com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Request;

import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Obj.CategoryObj;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SecondLevelCategoryRequest {

    @GET("/body/{gender}/{parent_id}/categories")
    Call<CategoryObj> secondLevelCategory(@Path("gender") String gender,
                                          @Path("parent_id") int patent_id);
}
