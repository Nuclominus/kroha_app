package com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Obj;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryObj {
    boolean status;
    String code;
    public JsonArray data;

    private int id;
    private String title;
    private int parent_id;
    private String gender;

    CategoryObj(){}

    public static List<CategoryObj> arrayCategoryObjFromData(String str) {

        Type listType = new TypeToken<ArrayList<CategoryObj>>() {
        }.getType();

        return new Gson().fromJson(str, listType);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public JsonArray getData(){
        return data;
    }
}
