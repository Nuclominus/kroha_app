package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@DatabaseTable(tableName = "ArticleContain")
public class ArticleContain {

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "id")
    public int id;

    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER)
    public int article_id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    public String type;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    public String value;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    public int priority;

    public static List<ArticleContain> newInstance(JSONArray obj) {
        ArrayList<ArticleContain> texts = new ArrayList<>(obj.length());
        for (int i = 0; i < obj.length(); i++) {
            ArticleContain articleContain = new ArticleContain();
            if (obj != null) {
                articleContain.id = obj.optJSONObject(i).optInt("id");
                articleContain.article_id = obj.optJSONObject(i).optInt("article_id");
                articleContain.type = obj.optJSONObject(i).optString("type");
                articleContain.value = obj.optJSONObject(i).optString("value");
                articleContain.priority = obj.optJSONObject(i).optInt("priority");
            }
            texts.add(articleContain);
        }
        Collections.sort(texts, new Comparator<ArticleContain>() {
            @Override
            public int compare(ArticleContain o1, ArticleContain o2) {
                if (o1.id > o2.id) return 1;
                else if (o1.id < o2.id) return -1;
                else return 0;
            }
        });
        return texts;
    }
}
