package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Logic;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.mukesh.MarkdownView;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleContain;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleObj;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Request.ArticleRequest;
import com.nuclominus.kroha.Utils.AppConsts;
import com.nuclominus.kroha.Utils.NetConnect;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleLogic {

    private ArticleObj articleObj;
    private Context context;

    public interface ArticleRequestCallback {
        public void successArticle(ArticleObj articleObj);
        public void failArticle();
    }

    public void getArticles(Context context, Bundle bundle, final ArticleRequestCallback callback) {
        this.context = context;
        ArticleRequest articlesRequest = NetConnect.getInstance(context).getRetrofit().create(ArticleRequest.class);
        Call<ArticleObj> response = articlesRequest.article(bundle.getString(AppConsts.GENDER),
                bundle.getInt(AppConsts.PARENT_ID),
                bundle.getInt(AppConsts.CATEGORY_ID),
                bundle.getInt(AppConsts.ARTICLE_ID));
        response.enqueue(new Callback<ArticleObj>() {
            @Override
            public void onResponse(Call<ArticleObj> call, Response<ArticleObj> response) {
                Log.d(ArticleLogic.class.getCanonicalName(), response.body().toString());
                articleObj = ArticleObj.objectFromData(response.body().data.toString());
                callback.successArticle(articleObj);
            }

            @Override
            public void onFailure(Call<ArticleObj> call, Throwable t) {
                callback.failArticle();
            }
        });
    }

    public void setViewData(TextView articleTitle, LinearLayout mainLayout) {
        articleTitle.setText(articleObj.getTitle());
        createArticle(articleObj, mainLayout);
    }

    public void createArticle(ArticleObj list, LinearLayout mainLayout) {
        for(ArticleContain contain : list.getArticleContain()) {
            if (contain.type.equals("text")) {
                createTextBlockHTML(contain.value, mainLayout);
            } else {
                try {
                    createVideoBlock(contain.value, mainLayout);
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }
    }

    private void createVideoBlock(final String link, LinearLayout view) {
        ImageView imageView = new ImageView(context);
        Glide.with(context).load("http://img.youtube.com/vi/" + link + "/1.jpg").into(imageView);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+link));
                intent.putExtra("force_fullscreen",true);
                context.startActivity(intent);
            }
        });
        view.addView(imageView);
    }

    private void createTextBlockHTML(String text, LinearLayout viewMain){
        MarkdownView markdownView = new MarkdownView(context);
        markdownView.setMarkDownText(text);
        viewMain.addView(markdownView);
    }
}
