package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj;

import com.google.gson.JsonObject;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

@DatabaseTable(tableName = "ArticleObj")
public class ArticleObj {
    boolean status;
    String code;
    public JsonObject data;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "id")
    private int id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String title;

    private List<ArticleContain> articleContain;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String hash;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private Pivot pivot;

    public static ArticleObj objectFromData(String str) {
        ArticleObj article = new ArticleObj();
        if (str != null && !str.equals(""))
            try {
                JSONObject object = new JSONObject(str);
                article.setId(object.optInt("id"));
                article.setTitle(object.optString("title"));
                article.setHash(object.optString("hash"));
                article.setId(object.optInt("id"));
                article.setPivot(Pivot.newInstance(new JSONObject(object.optString("pivot"))));
                article.setArticleContain(ArticleContain.newInstance(new JSONArray(object.optString("text"))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return article;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public List<ArticleContain> getArticleContain() {
        return articleContain;
    }

    public void setArticleContain(List<ArticleContain> articleContain) {
        this.articleContain = articleContain;
    }

    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }
}

class Pivot {

    int categoryId;
    int articleId;

    public static Pivot newInstance(JSONObject obj) {
        Pivot pivot = new Pivot();
        if (obj != null) {
            pivot.categoryId = obj.optInt("category_id");
            pivot.articleId = obj.optInt("article_id");
        }
        return pivot;
    }
}

