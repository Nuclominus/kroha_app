package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Obj;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ArticlesObj {
    boolean status;
    String code;
    public JsonArray data;

    private int id;
    private String title;
    private String hash;

    ArticlesObj(){}

    public static List<ArticlesObj> arrayArticlesObjFromData(String str) {

        Type listType = new TypeToken<ArrayList<ArticlesObj>>() {}.getType();

        return new Gson().fromJson(str, listType);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String gender) {
        this.hash = gender;
    }

    public JsonArray getData(){
        return data;
    }
}
