package com.nuclominus.kroha.Screens.SetsMenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;

public class SetsFragment extends ParentListFragment {


    public SetsFragment() {}


    public static SetsFragment newInstance() {
        SetsFragment fragment = new SetsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_abclist, container, false);
    }

    @Override
    public void setAdapter() {
    }

    @Override
    public void OnListItemClick(int parent_id) {
    }

}
