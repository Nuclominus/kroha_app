package com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Logic;


import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Obj.CategoryObj;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Request.SecondLevelCategoryRequest;
import com.nuclominus.kroha.Utils.LogWriter.LogWriter;
import com.nuclominus.kroha.Utils.NetConnect;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecondLevelCategoryLogic {

    private List<CategoryObj> arrayList = new ArrayList<>();

    public interface SecondLevelCategoryRequestCallback {
        public void successSecondLevelCategory(List<CategoryObj> categoryObjs);
        public void failSecondLevelCategory();
    }

    public void getCategoryData(Context context, int parent_id, String gender, final SecondLevelCategoryRequestCallback callback) {

        final SecondLevelCategoryRequest category = NetConnect.getInstance(context).getRetrofit().create(SecondLevelCategoryRequest.class);
        Call<CategoryObj> response = category.secondLevelCategory(gender, parent_id);
        response.enqueue(new Callback<CategoryObj>() {
            @Override
            public void onResponse(Call<CategoryObj> call, Response<CategoryObj> response) {
                CategoryObj categoryObj = response.body();
                Log.d(SecondLevelCategoryLogic.class.getCanonicalName(), SecondLevelCategoryLogic.class.getCanonicalName());
                try {
                    if (categoryObj != null)
                        arrayList = CategoryObj.arrayCategoryObjFromData(categoryObj.data.toString());
                    else
                        LogWriter.d(response.errorBody().toString());
                } catch (Exception e) {
                    LogWriter.e(e);
                    Crashlytics.logException(e);
                }
                callback.successSecondLevelCategory(arrayList);
            }

            @Override
            public void onFailure(Call<CategoryObj> call, Throwable t) {
                Log.d(SecondLevelCategoryLogic.class.getCanonicalName(), SecondLevelCategoryLogic.class.getCanonicalName());
                callback.failSecondLevelCategory();
            }
        });
    }

    public CategoryObj getCategory(int position) {
        return arrayList.get(position);
    }
}
