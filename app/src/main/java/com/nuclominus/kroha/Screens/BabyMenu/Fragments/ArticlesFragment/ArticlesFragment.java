package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Adapters.ABCListAdapter;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Logic.ArticlesLogic;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Obj.ArticlesObj;
import com.nuclominus.kroha.Utils.AppConsts;

import java.util.ArrayList;
import java.util.List;

public class ArticlesFragment extends ParentListFragment implements ArticlesLogic.ArticlesRequestCallback {

    private ABCListAdapter adapter;
    private ArticlesLogic logic;

    public ArticlesFragment() {
    }

    public static ArticlesFragment newInstance() {
        ArticlesFragment fragment = new ArticlesFragment();
        return fragment;
    }

    @Override
    public void setAdapter() {
        logic = new ArticlesLogic();
        logic.getArticles(getActivity(), bundle, this);
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_abclist, container, false);
    }

    @Override
    public void successArticles(List<ArticlesObj> categoryObjs) {
        if (categoryObjs != null && categoryObjs.size() > 0) {
            adapter = new ABCListAdapter(new ArrayList<>(categoryObjs), getActivity());
            setNoDataMessage(false);
        } else {
            setNoDataMessage(true);
        }
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void failArticles() {}

    @Override
    public void OnListItemClick(int parent_id) {
        bundle.putInt(AppConsts.ARTICLE_ID, logic.getArticle(parent_id).getId());
        onMenuItemClickListener.OnClickMenuItem(5);
    }
}
