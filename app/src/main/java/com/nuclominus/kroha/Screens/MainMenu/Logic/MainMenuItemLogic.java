package com.nuclominus.kroha.Screens.MainMenu.Logic;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.R;

import me.grantland.widget.AutofitTextView;

public class MainMenuItemLogic implements View.OnClickListener {

    private OnMenuItemClickListener listener;
    private View btn1, btn2, btn3;
    private Context context;

    public MainMenuItemLogic(Context context, View menuView, OnMenuItemClickListener listener) {
        btn1 = menuView.findViewById(R.id.iBtn_category_1);
        btn2 = menuView.findViewById(R.id.iBtn_category_2);
        btn3 = menuView.findViewById(R.id.iBtn_category_3);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);

        this.context = context;
        this.listener = listener;

        initViewButton();
    }

    private void initViewButton() {
        if (context != null) {
            ((ImageView) btn1.findViewById(R.id.iV_btn_category_icon)).setImageResource(R.drawable.icon_1_2);
            ((ImageView) btn2.findViewById(R.id.iV_btn_category_icon)).setImageResource(R.drawable.icon_2_2);
            ((ImageView) btn3.findViewById(R.id.iV_btn_category_icon)).setImageResource(R.drawable.icon_3_2);

            ((AutofitTextView) btn1.findViewById(R.id.btn_category_title)).setText(context.getString(R.string.baby_book));
            ((AutofitTextView) btn2.findViewById(R.id.btn_category_title)).setText(context.getString(R.string.abc_symptoms));
            ((AutofitTextView) btn3.findViewById(R.id.btn_category_title)).setText(context.getString(R.string.parents_control));

        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn1) {
            listener.OnClickMenuItem(1);
        } else if (v == btn2) {
            listener.OnClickMenuItem(2);
        } else if (v == btn3) {
            listener.OnClickMenuItem(3);
        }
    }
}
