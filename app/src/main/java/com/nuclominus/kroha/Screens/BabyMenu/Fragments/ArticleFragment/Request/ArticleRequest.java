package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Request;

import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleObj;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ArticleRequest {

    @GET("/body/{gender}/{parent_id}/categories/{category_id}/articles/{article_id}")
    Call<ArticleObj> article(@Path("gender") String gender,
                             @Path("parent_id") int parent_id,
                             @Path("category_id") int category_id,
                             @Path("article_id") int article_id
    );


}
