package com.nuclominus.kroha.Screens.Login.Logic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.nuclominus.kroha.Answers.AswToken;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Login.Request.LoginRequest;
import com.nuclominus.kroha.Utils.NetConnect;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginLogic {

    public interface LoginProgress{
        public void successLogin();
        public void failLogin();
    }


    private Context context;
    private LoginProgress interfaceProgress;
    private View mProgressView;
    private View mLoginFormView;
    private NetConnect netConnect;


    public LoginLogic(Context context, LoginProgress interfaceProgress, View mProgressView, View mLoginFormView){
        this.context = context;
        this.interfaceProgress = interfaceProgress;
        this.mProgressView  = mProgressView;
        this.mLoginFormView = mLoginFormView;

        netConnect = NetConnect.getInstance(context);
    }

    public void attemptLogin(AutoCompleteTextView mPhoneView, EditText mPasswordView) {

        mPhoneView.setError(null);
        mPasswordView.setError(null);

        String phone = mPhoneView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(context.getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(phone)) {
            mPhoneView.setError(context.getString(R.string.error_field_required));
            focusView = mPhoneView;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            mPhoneView.setError(context.getString(R.string.error_invalid_phone));
            focusView = mPhoneView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);

            try {
                LoginRequest token = netConnect.getRetrofit().create(LoginRequest.class);
                Call<AswToken> response = token.loginUser(phone, password);
                response.enqueue(new Callback<AswToken>() {
                    @Override
                    public void onResponse(Call<AswToken> call, Response<AswToken> response) {
                        AswToken token = response.body();
                        if (token != null)
                            token.saveToken();
                        interfaceProgress.successLogin();
                    }

                    @Override
                    public void onFailure(Call<AswToken> call, Throwable t) {
                        Log.d("ERROR", t.getLocalizedMessage());
                        interfaceProgress.failLogin();
                    }
                });
            } catch (NullPointerException e){
                Log.e("no retrofit obj error", e.getLocalizedMessage());
                interfaceProgress.successLogin();
            }
        }
    }

    private boolean isPhoneValid(String phone) {
        return phone.length() == 12 ? true : false;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
