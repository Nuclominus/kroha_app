package com.nuclominus.kroha.Screens.BabyMenu.Fragments.BabyFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.Parents.ParentFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.BabyFragment.Logic.MenuBaby;
import com.nuclominus.kroha.Utils.AppConsts;
import com.nuclominus.kroha.Utils.ImageUtil;

public class BabyFragment extends ParentFragment implements OnMenuItemClickListener {

    private MenuBaby menuBaby;
    private Button btn_organs, btn_skin;
    private int organ;

    public BabyFragment() {
    }

    public static BabyFragment newInstance() {
        BabyFragment fragment = new BabyFragment();
        return fragment;
    }

    @Override
    public void updateUI(Bundle bundle) {
        super.updateUI(bundle);

        final View babyView = rootView.findViewById(R.id.frameL_baby);
        menuBaby = new MenuBaby(getActivity(), babyView, this, bundle);
        menuBaby.setGender(bundle.getString(AppConsts.GENDER));
        if (organ != 0) {
            menuBaby.chooseOrgan(organ);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateUI(bundle);

        btn_organs = (Button) rootView.findViewById(R.id.btn_organs);
        btn_skin = (Button) rootView.findViewById(R.id.btn_skin);
        btn_skin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectButton();
                btn_skin.setTextColor(ImageUtil.getColor(getActivity(), android.R.color.white));
                btn_skin.setBackgroundResource(R.color.button_organ);
                menuBaby.dropSelect();
                bundle.putInt(AppConsts.PARENT_ID, 12);
                onMenuItemClickListener.OnClickMenuItem(3);
            }
        });

        btn_organs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle.putInt(AppConsts.PARENT_ID, organ);
                onMenuItemClickListener.OnClickMenuItem(3);
            }
        });
    }

    @Nullable
    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_baby, container, false);
    }

    @Override
    public void OnClickMenuItem(int position) {
        switch (position) {

            case 1: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_hear));
            }
            break;

            case 2: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_eyes));
            }
            break;

            case 3: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_nose));
            }
            break;

            case 4: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_ears));
            }
            break;


            case 5: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_mouth));
            }
            break;

            case 6: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_neck));
            }
            break;

            case 7: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_arms));
            }
            break;

            case 8: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_chest));
            }
            break;

            case 9: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_abdomen));
            }
            break;

            case 10: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_genitalia));
            }
            break;

            case 11: {
                deselectButton();
                btn_organs.setText(getActivity().getString(R.string.string_organ_legs));
            }
            break;

        }
        organ = position;
        btn_organs.setVisibility(View.VISIBLE);
    }

    private void deselectButton() {
        btn_organs.setVisibility(View.INVISIBLE);
        btn_skin.setBackgroundResource(android.R.color.transparent);
        btn_skin.setTextColor(ImageUtil.getColor(getActivity(), R.color.button_organ));
    }

}
