package com.nuclominus.kroha.Screens.BabyMenu;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.Parents.ParentFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ABCListFragment.ABCListFragment;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.ArticleFragment;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.ArticlesFragment;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.BabyFragment.BabyFragment;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.SecondLevelCategoryFragment;
import com.nuclominus.kroha.Screens.BabyMenu.Logic.MenuFragmentLogic;

public class MenuFragmentActivity extends ParentActivity implements OnMenuItemClickListener, ParentActivity.DrawerInterface {

    enum SWITCH_ITEM {
        LIST,
        PICTURE
    }

    private MenuFragmentLogic menuFragmentLogic;
    private TextView item_sex, item_switch;
    private SWITCH_ITEM switch_item;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_menu_fragment);

        setToolbarTitle(getString(R.string.abc_symptoms).replace("\n", ""), android.R.color.white);
        initDrawer(this);

        item_sex = (TextView) findViewById(R.id.tV_menuitem_sex);
        item_switch = (TextView) findViewById(R.id.tV_menuitem_switch);

        final View menuView = findViewById(R.id.includeToolbarBottom);
        menuFragmentLogic = new MenuFragmentLogic(this, menuView, this, bundle);
        switch_item = SWITCH_ITEM.PICTURE;
        fm.addOnBackStackChangedListener(getListener());
        OnClickMenuItem(2);

        if (f != null) {
           router(f);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, f.FRAGMENT_TAG, f);
    }

    @Override
    public void OnDrawerItemSelect(int itemPosition) {
        switch (itemPosition - 1) {
            case 1: {

            }
            break;

            case 2: {
//                switch_item = SWITCH_ITEM.LIST;
                getVisibleFragment();
                if (f instanceof BabyFragment) {
                    setItem_switch();
                    menuFragmentLogic.setAction(2);
                    router(f);
                }
            }
            break;

            case 3: {

            }
            break;
        }
    }

    @Override
    public void OnClickMenuItem(int position) {
        getVisibleFragment();

        switch (position) {

            case 1: {
                if (f != null) {
                    if (bundle.getInt(getString(R.string.sex)) == 1) {
                        bundle.putInt(getString(R.string.sex), 2);
                    } else {
                        bundle.putInt(getString(R.string.sex), 1);
                    }
                    f.updateUI(bundle);
                }
            }
            break;

            case 2: {
                setItem_switch();
                mainDrawer.deselect();
                router(f);
            }
            break;

            case 3: {
                f = SecondLevelCategoryFragment.newInstance();
                f.callBackItemClick(this);
                f.setArguments(bundle);
                router(f);
            }
            break;

            case 4: {
                f = ArticlesFragment.newInstance();
                f.callBackItemClick(this);
                f.setArguments(bundle);
                router(f);
            }
            break;

            case 5: {
                f = ArticleFragment.newInstance();
                f.setArguments(bundle);
                router(f);
            }
            break;

        }
    }

    private void setItem_switch() {
        if (switch_item == SWITCH_ITEM.PICTURE) {
            f = BabyFragment.newInstance();
            f.setArguments(bundle);
            f.callBackItemClick(this);
            switch_item = SWITCH_ITEM.LIST;
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else if (switch_item == SWITCH_ITEM.LIST) {
            f = ABCListFragment.newInstance();
            f.callBackItemClick(this);
            f.setArguments(bundle);
            switch_item = SWITCH_ITEM.PICTURE;
        }
    }

    private void router(ParentFragment f) {
        if (f != null)
            fm.beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left, R.anim.anim_slide_in_right, R.anim.anim_slide_out_right)
                    .replace(R.id.contentFrame, f, f.getTag())
                    .addToBackStack(f.getTag())
                    .commit();
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();
                if (manager != null) {
                    getVisibleFragment();
                    if (f instanceof BabyFragment) {
                        switch_item = SWITCH_ITEM.LIST;
                        menuFragmentLogic.setAction(2);
                    }
                }
            }
        };
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
