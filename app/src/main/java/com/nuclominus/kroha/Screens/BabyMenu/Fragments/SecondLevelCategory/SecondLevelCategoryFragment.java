package com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Adapters.ABCListAdapter;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Logic.SecondLevelCategoryLogic;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.SecondLevelCategory.Obj.CategoryObj;
import com.nuclominus.kroha.Utils.AppConsts;

import java.util.ArrayList;
import java.util.List;

public class SecondLevelCategoryFragment extends ParentListFragment implements SecondLevelCategoryLogic.SecondLevelCategoryRequestCallback {

    private ABCListAdapter adapter;
    private SecondLevelCategoryLogic logic;

    public SecondLevelCategoryFragment() {
    }

    public static SecondLevelCategoryFragment newInstance() {
        SecondLevelCategoryFragment fragment = new SecondLevelCategoryFragment();
        return fragment;
    }

    public void setAdapter() {
        logic = new SecondLevelCategoryLogic();
        logic.getCategoryData(getActivity(),
                bundle.getInt(AppConsts.PARENT_ID),
                bundle.getString(AppConsts.GENDER),
                this);
    }

    @Nullable
    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_abclist, container, false);
    }

    @Override
    public void successSecondLevelCategory(List<CategoryObj> categoryObjs) {
        if (categoryObjs != null && categoryObjs.size() > 0) {
            adapter = new ABCListAdapter(new ArrayList<>(categoryObjs), getActivity());
            setNoDataMessage(false);
        }else {
            setNoDataMessage(true);
        }
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void failSecondLevelCategory() {
    }

    @Override
    public void OnListItemClick(int parent_id) {
        bundle.putInt(AppConsts.CATEGORY_ID, logic.getCategory(parent_id).getId());
        onMenuItemClickListener.OnClickMenuItem(4);
    }
}
