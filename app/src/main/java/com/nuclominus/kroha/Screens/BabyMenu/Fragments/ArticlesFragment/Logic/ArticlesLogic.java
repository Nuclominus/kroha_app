package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Logic;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.nuclominus.kroha.Answers.AswToken;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Obj.ArticlesObj;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Request.ArticlesRequest;
import com.nuclominus.kroha.Utils.AppConsts;
import com.nuclominus.kroha.Utils.NetConnect;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticlesLogic {

    private List<ArticlesObj> arrayList;

    public interface ArticlesRequestCallback{
        public void successArticles(List<ArticlesObj> categoryObjs);
        public void failArticles();
    }

    public void getArticles(Context context, Bundle bundle, final ArticlesRequestCallback callback){
        ArticlesRequest articlesRequest = NetConnect.getInstance(context).getRetrofit().create(ArticlesRequest.class);
        Call<ArticlesObj> response = articlesRequest.articles(bundle.getString(AppConsts.GENDER),
                bundle.getInt(AppConsts.PARENT_ID), bundle.getInt(AppConsts.CATEGORY_ID));

        response.enqueue(new Callback<ArticlesObj>() {
            @Override
            public void onResponse(Call<ArticlesObj> call, Response<ArticlesObj> response) {
                Log.d(ArticlesLogic.class.getCanonicalName(), response.body().toString());
                arrayList = ArticlesObj.arrayArticlesObjFromData(response.body().data.toString());
                callback.successArticles(arrayList);
            }

            @Override
            public void onFailure(Call<ArticlesObj> call, Throwable t) {
                callback.failArticles();
            }
        });

    }

    public ArticlesObj getArticle(int position){
        return arrayList.get(position);
    }
}
