package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.mukesh.MarkdownView;
import com.nuclominus.kroha.BuildConfig;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleContain;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleObj;
import com.nuclominus.kroha.Utils.AppConsts;


/**
 * Created by Nuclominus on 29.09.2016.
 */

public class ArticleViewAdapter extends RecyclerView.Adapter<ArticleViewAdapter.ViewHolder> {

    private ArticleObj list;
    private Context context;

    public ArticleViewAdapter(ArticleObj list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ArticleObj obj = list;
        holder.mainLayout.removeAllViews();
        ArticleContain articleContain = obj.getArticleContain().get(position);
        if (articleContain.type.equals("text")) {
            createTextBlockHTML(articleContain.value, holder.mainLayout);
        } else {
            try {
                createVideoBlock(articleContain.value, holder.mainLayout);
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    @Override
    public int getItemCount() {
        if (list != null)
            return list.getArticleContain().size();
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mainLayout = (LinearLayout) itemView.findViewById(R.id.frameArticle);
        }
    }

    private void createVideoBlock(final String link, LinearLayout view) {
        final YouTubeThumbnailView youTubeThumbnailView = new YouTubeThumbnailView(context);
        youTubeThumbnailView.setMinimumHeight(700);
        final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
            @Override
            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

            }

            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {

            }
        };

        youTubeThumbnailView.initialize("AIzaSyAQr0UMbbfS7SF1qOtcRiFJTjfEbsaxuao", new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                youTubeThumbnailLoader.setVideo(link);
                youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                //write something for failure
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyAQr0UMbbfS7SF1qOtcRiFJTjfEbsaxuao", link,
                        100,     //after this time, video will start automatically
                        false,               //autoplay or not
                        true);
                context.startActivity(intent);

            }
        });
        view.setBackgroundColor(Color.BLACK);
        view.addView(youTubeThumbnailView);
    }


    private void createTextBlock(String text, LinearLayout viewMain) {
        WebView view = new WebView(context);
        view.clearCache(true);
        view.clearFormData();

        view.setWebChromeClient(new WebChromeClient());

        final WebSettings webSettings = view.getSettings();
        webSettings.setDefaultFontSize(26);

        view.getSettings().setAllowFileAccess(true);
        view.getSettings().setPluginState(WebSettings.PluginState.ON);
        view.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        view.setWebViewClient(new WebViewClient());
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setLoadWithOverviewMode(true);
        view.getSettings().setUseWideViewPort(true);

        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheEnabled(true);
        webSettings.setBlockNetworkImage(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);

        view.loadDataWithBaseURL(BuildConfig.API_URL, text, AppConsts.ENCODE, AppConsts.CODING, "");
        viewMain.addView(view);
    }

    private void createTextBlockHTML(String text, LinearLayout viewMain){
        MarkdownView markdownView = new MarkdownView(context);
        markdownView.setMarkDownText(text);
        viewMain.addView(markdownView);
    }

//    private void createTextBlock(String text, LinearLayout view) {
//        TextView textView = new TextView(context);
//        textView.setArticleContain(Html.fromHtml(text));
//        view.addView(textView);
//    }
}
