package com.nuclominus.kroha.Screens.Login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Login.Logic.LoginLogic;
import com.nuclominus.kroha.Screens.MainMenu.MainMenuActivity;
import com.nuclominus.kroha.Screens.Register.RegisterActivity;


public class LoginActivity extends ParentActivity implements LoginLogic.LoginProgress {

    private AutoCompleteTextView mPhoneView;
    private EditText mPasswordView;
    private LoginLogic loginLogic;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_login);

        mPhoneView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        loginLogic = new LoginLogic(this,
                this,
                findViewById(R.id.login_form),
                findViewById(R.id.login_progress));

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    loginLogic.attemptLogin(mPhoneView, mPasswordView);
                    return true;
                }
                return false;
            }
        });


        Button mSignInButton = (Button) findViewById(R.id.btn_sign_in);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                loginLogic.attemptLogin(mPhoneView, mPasswordView);
            }
        });

        Button register = (Button) findViewById(R.id.btn_register);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        request();

    }


    public void request(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        240);
            }
        }
    }


    @Override
    public void successLogin() {
        startActivity(new Intent(LoginActivity.this, MainMenuActivity.class));
        finish();
    }

    @Override
    public void failLogin() {

    }
}

