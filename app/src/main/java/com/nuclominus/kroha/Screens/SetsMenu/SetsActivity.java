package com.nuclominus.kroha.Screens.SetsMenu;

import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;

public class SetsActivity extends ParentActivity {

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_menu_fragment);
        f = SetsFragment.newInstance();
        setFragment();

    }

    private void setFragment(){
        if (f != null)
            fm.beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left, R.anim.anim_slide_in_right, R.anim.anim_slide_out_right)
                    .replace(R.id.contentFrame, f, f.getTag())
                    .addToBackStack(f.getTag())
                    .commit();
    }

}
