package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Request;

import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticlesFragment.Obj.ArticlesObj;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ArticlesRequest {

    @GET("/body/{gender}/{parent_id}/categories/{category_id}/articles")
    Call<ArticlesObj> articles(@Path("gender") String gender,
                               @Path("parent_id") int parent_id,
                               @Path("category_id") int category_id);
}
