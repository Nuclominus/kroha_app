package com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nuclominus.kroha.Parents.ParentFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Adapter.ArticleViewAdapter;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Logic.ArticleLogic;
import com.nuclominus.kroha.Screens.BabyMenu.Fragments.ArticleFragment.Obj.ArticleObj;
import com.nuclominus.kroha.Utils.Thread.PoolThread;

public class ArticleFragment extends ParentFragment implements ArticleLogic.ArticleRequestCallback {

    private ArticleLogic logic;
    private ImageButton share, answer;
    private TextView articleTitle;
    private ArticleViewAdapter adapter;
    private LinearLayout article_container;
    public ArticleFragment() {
    }

    public static ArticleFragment newInstance() {
        return new ArticleFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FRAGMENT_TAG = getClass().getName();

        logic = new ArticleLogic();
        AsyncTask<Void,Void,Void> loadTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                logic.getArticles(getActivity(), bundle, ArticleFragment.this);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        };
        loadTask.executeOnExecutor(PoolThread.EXECUTOR);

        article_container = (LinearLayout) rootView.findViewById(R.id.article_contain);

//        recyclerView = (RecyclerView) rootView.findViewById(R.id.rV_list);
        articleTitle = (TextView) rootView.findViewById(R.id.tV_article_title);
        share = (ImageButton) rootView.findViewById(R.id.btn_article_share);
        answer = (ImageButton) rootView.findViewById(R.id.btn_article_answer);


//        LogWriter.sendLogs(getActivity());
    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_article, container, false);
    }

    @Override
    public void successArticle(ArticleObj articleObj) {
        logic.setViewData(articleTitle, article_container);
//        if (articleObj != null) {
//            adapter = new ArticleViewAdapter(articleObj, getActivity());
//        }
//        recyclerView.setAdapter(adapter);
    }

    @Override
    public void failArticle() {

    }

    @Override
    public void onStop() {
        article_container.removeAllViews();
        super.onStop();
    }
}
