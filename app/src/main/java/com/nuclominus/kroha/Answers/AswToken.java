package com.nuclominus.kroha.Answers;

import com.nuclominus.kroha.Utils.AppConsts;
import com.nuclominus.kroha.Utils.PreferencesManager;

public class AswToken {
    boolean status;
    String token;

    public boolean isStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public void saveToken() {
        if (getToken() != null)
            PreferencesManager.getInstance().setStringValue(AppConsts.TOKEN, getToken());
    }

    public static String loadToken() {
        return PreferencesManager.getInstance().getStringValue(AppConsts.TOKEN);
    }
}
