package com.nuclominus.kroha.Parents;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Utils.ImageUtil;

import java.util.List;

import me.grantland.widget.AutofitHelper;
import me.grantland.widget.AutofitTextView;

public abstract class ParentActivity extends AppCompatActivity {

    public interface DrawerInterface {
        public void OnDrawerItemSelect(int itemPosition);
    }

    public Toolbar toolbar;
    private PrimaryDrawerItem mailMenuItem, listMenuItem;
    public Bundle bundle;
    public Drawer mainDrawer;
    private DrawerInterface drawerInterface;
    public FragmentManager fm = getSupportFragmentManager();
    public ParentFragment f = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getIntent().getExtras();
        if (savedInstanceState != null) {
            f = (ParentFragment) getSupportFragmentManager().getFragment(savedInstanceState, f.FRAGMENT_TAG);
        }
        initUI();
        loadBG();
        changeFont(getWindow().getDecorView().getRootView());
        if (findViewById(R.id.toolbar) != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        showLeftAnimation(this);
    }

    protected void initUI() {
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public void setToolbarTitle(String title, int color) {
        AutofitHelper.create(((TextView) findViewById(R.id.main_toolbar_title)));
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setText(title);
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTextColor(ImageUtil.getColor(this, color));
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTextSize(22);
//        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTypeface(FontsUtil.getInstance().getLatoBold());
    }

    public void initDrawer(final DrawerInterface drawerInterface) {

        this.drawerInterface = drawerInterface;

        mailMenuItem = new PrimaryDrawerItem().withName(R.string.messages).withIcon(R.drawable.icon_mail).withTextColor(ImageUtil.getColor(this, android.R.color.white));
        listMenuItem = new PrimaryDrawerItem().withName(R.string.list).withIcon(R.drawable.icon_list).withTextColor(ImageUtil.getColor(this, android.R.color.white));
        ;

        mainDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withSliderBackgroundColor(ImageUtil.getColor(this, R.color.drawer_bg_color))
                .addDrawerItems(
                        mailMenuItem,
                        new DividerDrawerItem(),
                        listMenuItem,
                        new DividerDrawerItem()
                )
                .withHeader(R.layout.drawer_header)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        mainDrawer.closeDrawer();
                        drawerInterface.OnDrawerItemSelect(position);
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withSelectedItem(-1)
                .build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                result = true;
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    protected void changeFont(View v) {
//        FontsUtil.overrideFonts(this, v);
    }

    protected void loadBG() {
        if (findViewById(R.id.iv_bg) != null)
            ((ImageView) findViewById(R.id.iv_bg)).setImageBitmap(ImageUtil.getBg(this));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        int count = fm.getBackStackEntryCount();
        if (count > 1) {
            fm.popBackStack();
        } else {
            super.finish();
            showRightAnimation(this);
        }
    }

    public void getVisibleFragment() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    f = (ParentFragment) fragment;
            }
        }
    }

    public static void showLeftAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public static void showRightAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }

}

