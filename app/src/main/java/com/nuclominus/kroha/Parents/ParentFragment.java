package com.nuclominus.kroha.Parents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;

public abstract class ParentFragment extends Fragment{

    protected View rootView;
    protected Bundle bundle;
    public OnMenuItemClickListener onMenuItemClickListener;
    public static String FRAGMENT_TAG = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bundle = getArguments();
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's state here
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = getView(inflater, container);
        return rootView;
    }

    public void callBackItemClick(OnMenuItemClickListener listener){
        onMenuItemClickListener = listener;
    }

    public void updateUI(Bundle bundle){}

    public View getView(LayoutInflater inflater, ViewGroup container){
        return null;
    }
}
