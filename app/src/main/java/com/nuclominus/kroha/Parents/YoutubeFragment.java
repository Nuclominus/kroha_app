package com.nuclominus.kroha.Parents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;

/**
 * Created by Nuclominus on 16.10.2016.
 */

public class YoutubeFragment extends YouTubePlayerSupportFragment {

    protected View rootView;
    protected Bundle bundle;
    public OnMenuItemClickListener onMenuItemClickListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bundle = getArguments();
        super.onCreate(savedInstanceState);
        final YouTubeInitializationResult result = YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getContext());

        if (result != YouTubeInitializationResult.SUCCESS) {
            //If there are any issues we can show an error dialog.
            result.getErrorDialog(getActivity(), 0).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = getView(inflater, container);
        return rootView;
    }

    public void callBackItemClick(OnMenuItemClickListener listener) {
        onMenuItemClickListener = listener;
    }

    public void updateUI(Bundle bundle) {
    }

    public View getView(LayoutInflater inflater, ViewGroup container) {
        return null;
    }

}
