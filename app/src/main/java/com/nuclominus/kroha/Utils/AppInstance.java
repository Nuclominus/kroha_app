package com.nuclominus.kroha.Utils;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.nuclominus.kroha.Utils.DBDao.BaseFactory;

import io.fabric.sdk.android.Fabric;

public class AppInstance extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PreferencesManager.initializeInstance(getApplicationContext());
        BaseFactory.setHelper(this);
//        FontsUtil.initializeInstance(getApplicationContext());
    }
}
