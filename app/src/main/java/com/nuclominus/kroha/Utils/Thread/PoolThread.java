package com.nuclominus.kroha.Utils.Thread;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


/**
 * Created by Nuclominus on 15.10.2016.
 */

public class PoolThread {

    private final static int CORE_POOL_SIZE = 5;

    public static final ExecutorService EXECUTOR;

    static {
        int corePoolSize = ThreadHelper.getCorePoolSize();
        EXECUTOR = new ThreadPoolExecutor(corePoolSize, ThreadHelper.getPoolSize(corePoolSize),
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    private PoolThread(){}

    private static class ThreadHelper {

        private static int getCorePoolSize(){
            int cores = getNumCores();
            return cores > CORE_POOL_SIZE ? cores : CORE_POOL_SIZE;
        }

        private static int getPoolSize(int corePoolSize){
            return corePoolSize * 3;
        }

        public static int getNumCores() {
            try {
                File dir = new File("/sys/devices/system/cpu/");
                File[] files = dir.listFiles(new CpuFilter());
                return files.length;
            } catch (Exception ex) {
//                LogWriter.e(ex);
                return 4;
            }
        }

        private static class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                return Pattern.matches("cpu[0-9]+", pathname.getName());
            }
        }
    }

}
