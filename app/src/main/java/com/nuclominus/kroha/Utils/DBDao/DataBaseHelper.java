package com.nuclominus.kroha.Utils.DBDao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.nuclominus.kroha.Utils.AppConsts;
import com.nuclominus.kroha.Utils.Profile.OwnerProfile;

import java.sql.SQLException;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DataBaseHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    private DeviceDAO deviceDao = null;

    public DataBaseHelper(Context context) {
        super(context, AppConsts.Base.PROFILE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, OwnerProfile.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + AppConsts.Base.PROFILE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {
            TableUtils.dropTable(connectionSource, OwnerProfile.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + AppConsts.Base.PROFILE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    public DeviceDAO getProfileDao() throws SQLException {
        if (deviceDao == null) {
            deviceDao = new DeviceDAO(getConnectionSource(), OwnerProfile.class);
        }
        return deviceDao;
    }

    public void clearProfileDao() throws SQLException {
        TableUtils.clearTable(getConnectionSource(), OwnerProfile.class);
    }

    public void createProfileDao(OwnerProfile device) throws SQLException {
        if (deviceDao == null) {
            deviceDao = new DeviceDAO(getConnectionSource(), OwnerProfile.class);
        }
        deviceDao.create(device);
    }

    @Override
    public void close() {
        super.close();
        deviceDao = null;
    }
}

