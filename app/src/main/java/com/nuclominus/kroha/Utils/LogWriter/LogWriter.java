package com.nuclominus.kroha.Utils.LogWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.nuclominus.kroha.Utils.Profile.OwnerProfile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class LogWriter implements ILogWriter {

    public static final String LOGS_EMAIL = "9DGRoman@gmail.com";
    private static final String FILE_NAME_LOG =  "/log.txt";
    private static final String FILE_NAME_LOG_ERROR =  "/errorLog.txt";
    private static final LogWriter INSTANCE;
    private static String _versionString;
    private static String _errorLog;

    static {
        INSTANCE = new LogWriter();
    }

    private ILogWriter _externalLogWriter;

    private void setExternalLogWriter(ILogWriter externalLogWriter) {
        _externalLogWriter = externalLogWriter;
    }

    @Override
    public void eW(Throwable ex) {
        eW("", ex);
    }

    @Override
    public void eW(String message, Throwable ex) {
        if (_externalLogWriter != null) {
            _externalLogWriter.eW(message, ex);
        }
    }

    @Override
    public void dW(String message) {
        if (_externalLogWriter != null) {
            _externalLogWriter.dW(message);
        }
    }

    @Override
    public void debugW(String message) {
        if (_externalLogWriter != null) {
            _externalLogWriter.debugW(message);
        }
    }

    public static void configure(ILogWriter externalLogWriter) {
        INSTANCE.setExternalLogWriter(externalLogWriter);
    }

    public static void e(Throwable ex) {
        INSTANCE.eW(ex);
    }

    public static void e(String message, Throwable ex) {
        INSTANCE.eW(message, ex);
    }

    public static void d(String message) {
        INSTANCE.dW(message);
    }

    public static void debug(String message) {
        INSTANCE.debugW(message);
    }

    public static void removeLogFile(String filePath) {
        File logFile = new File(filePath);
        if (logFile.exists())
            logFile.delete();
    }

    public static File flush(String filePath) {
        File result = new File(filePath);
        try {
            result.createNewFile();

            ProcessBuilder pb = new ProcessBuilder(new String[]{"logcat", "-d"});
            pb.redirectErrorStream(true);

            Process process = pb.start();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;

            StringBuilder log = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
                log.append('\n');
            }
            process.destroy();
            FileUtils.writeStringToFile(result, log.toString());
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return result;
    }

    public static String getDeviceName() {
        return android.os.Build.MODEL;
    }

    public static String getOSVersion() {
        return android.os.Build.VERSION.RELEASE;
    }

    public static String getVersion(Context context) {
        if (_versionString == null || _versionString.isEmpty()) {
            try {
                _versionString = "";
                _versionString = context.getApplicationContext().getPackageManager().getPackageInfo(context.getApplicationContext().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException ignored) {
            }
        }
        return _versionString;
    }

    public static double getDisplayInches(Context context) {
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        float screenWidth = displayMetrics.widthPixels / displayMetrics.xdpi;
        float screenHeight = displayMetrics.heightPixels / displayMetrics.ydpi;
        return Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));
    }

    public static int getNumCores() {
        try {
            File dir = new File("/sys/devices/system/cpu/");
            File[] files = dir.listFiles(new CpuFilter());
            return files.length;
        } catch (Exception ex) {
            LogWriter.e(ex);
            return 4;
        }
    }

    public static String getErrorLogPathInternal(String path) {
        if (_errorLog == null) {
            _errorLog = "Logs";
            _errorLog = FilenameUtils.concat(_errorLog, Environment.getExternalStorageDirectory().getAbsolutePath() + path);
            File file = new File(_errorLog);
            if (file.exists() && ((file.length() / 1024) / 1024) >= 5) {
                file.delete();
            }
        }
        return _errorLog;
    }

    private static boolean checkWriteExternalPermission(Context context)
    {
        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static void sendLogs(Activity activity) {

        if (activity != null && checkWriteExternalPermission(activity)) {

            File fileErrorLog = new File(getErrorLogPathInternal(FILE_NAME_LOG_ERROR));

            String logPath = getErrorLogPathInternal(FILE_NAME_LOG);
            LogWriter.flush(logPath);
            File fileLogCat = new File(logPath);

            try {
                fileErrorLog.createNewFile();
                fileLogCat.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            boolean fileErrorLogExist = fileErrorLog.exists();
            boolean fileLogCatExist = fileLogCat.exists();

            if (fileErrorLogExist || fileLogCatExist) {
                try {
                    final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{LOGS_EMAIL});
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Kroha User Log (Android)");
                    ArrayList<Uri> uris = new ArrayList<Uri>();
                    if (fileErrorLogExist) {
                        uris.add(Uri.fromFile(fileErrorLog));
                    }
                    if (fileLogCatExist) {
                        uris.add(Uri.fromFile(fileLogCat));
                    }
                    emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                    StringBuilder builder = new StringBuilder();

                    builder.append(String.format("User ID: %s\n", OwnerProfile.getInstance().getUserID()));
                    builder.append(String.format("Version: %s", getVersion(activity)));
                    builder.append(String.format("OS Version: %s\n", getOSVersion()));
                    builder.append(String.format("Device Name: %s\n", getDeviceName()));
                    builder.append(String.format("Inches: %s\n", getDisplayInches(activity)));
                    builder.append(String.format("Cores count: %s", getNumCores()));
                    ArrayList<CharSequence> parameters = new ArrayList<CharSequence>();
                    parameters.add(builder.toString());
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, parameters);
                    activity.startActivity(Intent.createChooser(emailIntent, "Sending email..."));
                } catch (Throwable t) {
                    Toast.makeText(activity.getApplicationContext(), "Request failed try again: " + t.toString(), Toast.LENGTH_LONG).show();
                    LogWriter.e(t);
                }
            }

        }
    }

    private static class CpuFilter implements FileFilter {
        @Override
        public boolean accept(File pathname) {
            return Pattern.matches("cpu[0-9]+", pathname.getName());
        }
    }
}
