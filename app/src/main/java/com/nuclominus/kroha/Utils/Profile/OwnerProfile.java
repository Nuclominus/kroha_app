package com.nuclominus.kroha.Utils.Profile;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Nuclominus on 06.10.2016.
 */
@DatabaseTable(tableName = "Profile")
public class OwnerProfile {

    public static OwnerProfile instance = null;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = "id")
    private String id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String name = "";

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String lastName = "";

    @DatabaseField(canBeNull = false, dataType = DataType.STRING)
    private String email = "";

    @DatabaseField(canBeNull = false, dataType = DataType.INTEGER)
    private int userID = 0;

    public static OwnerProfile getInstance(){
        if(instance == null){
            instance = new OwnerProfile();
        }
        return instance;
    }

    private OwnerProfile() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
