package com.nuclominus.kroha.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;

import com.nuclominus.kroha.Answers.AswToken;
import com.nuclominus.kroha.BuildConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetConnect {

    public static NetConnect instance = null;
    private Context context;
    private Retrofit retrofit;

    public static NetConnect getInstance(Context context) {
        if (instance == null) {
            instance = new NetConnect();
        }
        instance.context = context;
        return instance;
    }

    private NetConnect() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .addHeader("Authorization", "Bearer " + AswToken.loadToken())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        try {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        } catch (IllegalArgumentException e) {
            Log.e("Create retrofit error", e.getLocalizedMessage());
        }
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    @NonNull
    public static ArrayList<String> parseLinks(String text) {
        ArrayList<String> links = new ArrayList<>();
        Matcher m = Patterns.WEB_URL.matcher(text);
        while (m.find()) {
            String url = m.group();
            if (!links.contains(url)) {
                links.add(url);
            }
        }
       return links;
    }

    public static ArrayList<String> parceText(String text, ArrayList<String> links){
        ArrayList<String> splitText = new ArrayList<>();
        for (int i = 0; i < links.size(); i++) {
            if (splitText.size() > 0) {
                Collections.addAll(splitText, splitText.get(i).split(links.get(i)));
                splitText.remove(i);
            } else {
                Collections.addAll(splitText, text.split(links.get(i)));
            }
        }
        return splitText;
    }
}
