package com.nuclominus.kroha.Utils.DBDao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.nuclominus.kroha.Utils.Profile.OwnerProfile;

import java.sql.SQLException;
import java.util.List;

public class DeviceDAO extends BaseDaoImpl<OwnerProfile, Integer> {

    protected DeviceDAO(ConnectionSource connectionSource,
                        Class<OwnerProfile> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<OwnerProfile> getAllProfiles() throws SQLException {
        return this.queryBuilder().query();
    }

    public void removeProfileItem(Long id) throws SQLException {
        this.deleteBuilder().where().eq("id", id);
        this.deleteBuilder().delete();
    }

    public boolean checkExist(Long id) throws SQLException {
        Long having = this.queryBuilder().where().eq("id", id).countOf();
        if (having != null) {
            return true;
        }
        return false;
    }

}
