package com.nuclominus.kroha.Utils;

public class AppConsts {
    public static final String TOKEN = "app_token";
    public static final String GENDER = "gender";
    public static final String PARENT_ID = "parent_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String ARTICLE_ID = "article_id";
    public static final String ENCODE = "text/html";
    public static final String CODING = "utf-8";

    public class Base {
        public static final String PROFILE_NAME ="profile.db";
    }
}
